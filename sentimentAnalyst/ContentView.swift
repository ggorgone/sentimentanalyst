//
//  ContentView.swift
//  sentimentAnalyst
//
//  Created by Giovanni Gorgone on 13/05/2020.
//  Copyright © 2020 ggorgone. All rights reserved.
//

import SwiftUI
import NaturalLanguage

struct ContentView: View {
    @State private var sentence: String = ""
    @State private var sentiment: String = ""
    
    var body: some View {
        VStack{
            TextField("Let your brain flow", text: $sentence)
            Text(" \(sentiment)")
            Button("Evaluate"){
                self.sentimentScore()
            }
        }
    }
    
    private func sentimentScore(){
        let tagger = NLTagger(tagSchemes: [.sentimentScore])
        tagger.string = self.sentence
        
        // ask for the results
        let (sentiment, _) = tagger.tag(at: self.sentence.startIndex, unit: .paragraph, scheme: .sentimentScore)

        // read the sentiment back and print it
        let score = Double(sentiment?.rawValue ?? "0") ?? 0
        if score <= 0.3 && score >= -0.3{
            self.sentiment = "not great, not terrible"
        } else if score < 0 {
            self.sentiment = "oh, i see you probably have terrible boss"
        } else if score > 0 {
            self.sentiment = "good, they really appreciate you"
        }
        self.sentiment += " " + String(score)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
